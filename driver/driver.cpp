#include <windows.h>
#include <cstdio>

bool InjectDLL(HANDLE process, const char* dllPath, const char*& error)
{
	LPVOID dllPathAddress = VirtualAllocEx(process,
		0,
		strlen(dllPath),
		MEM_RESERVE | MEM_COMMIT,
		PAGE_EXECUTE_READWRITE);

	if (dllPathAddress == 0)
	{
		error = "Could not allocate memory on target process";
		return false;
	}

	if (!WriteProcessMemory(process, dllPathAddress, dllPath, strlen(dllPath), NULL))
	{
		error = "Could not write DLL path into target memory";
		return false;
	}

	HMODULE kernelModule = GetModuleHandle(TEXT("kernel32.dll"));

	if (!kernelModule)
	{
		error = "Could not find module 'kernel32.dll'";
		return false;
	}

	FARPROC LoadLibraryAFunction = GetProcAddress(kernelModule, "LoadLibraryA");

	if (!LoadLibraryAFunction)
	{
		error = "Could no locate the address of 'LoadLibraryA' in target process";
		return false;
	}

	HANDLE remoteThread = CreateRemoteThread(process, NULL, 0, (LPTHREAD_START_ROUTINE)LoadLibraryAFunction, dllPathAddress, 0, NULL);
	  
	if (!remoteThread)
	{
		error = "Failed to start remote thread in the target process";
		return false;
	}

	WaitForSingleObject(remoteThread, INFINITE);

	return true;
}

int main()
{
	STARTUPINFOA info = { };
	ZeroMemory(&info, sizeof(info));

	PROCESS_INFORMATION processInfo;

	if (!CreateProcessA("target.exe", NULL, NULL, NULL, TRUE, 0, NULL, NULL, &info, &processInfo))
	{
		printf("Failed to launch target process");
		return 0;
	}

	Sleep(4500);

	const char* error;
	if (!InjectDLL(processInfo.hProcess, "dll.dll", error))
	{
		printf(error);
		return 0;
	}

	WaitForSingleObject(processInfo.hProcess, INFINITE);

	DWORD exitcode;
	if (!GetExitCodeProcess(processInfo.hProcess, &exitcode))
	{
		printf("Could not get exit code from target process");
		return 0;
	}

	printf("Exit code from process %d\n", exitcode);
	
	CloseHandle(processInfo.hProcess);
	CloseHandle(processInfo.hThread);

	return 0;
}
